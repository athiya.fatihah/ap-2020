package id.ac.ui.cs.advprog.tutorial4.singleton.core;

public class HolyWish {

    private String wish;
    private static HolyWish holyWish;

    // TODO complete me with any Singleton approach
    public static HolyWish getInstance(){
        if(holyWish == null){
            holyWish = new HolyWish();
        }
        return holyWish;
    }

    public String getWish() {
        return wish;
    }

    public void setWish(String wish) {
        this.wish = wish;
    }

    @Override
    public String toString() {
        return wish;
    }
}
