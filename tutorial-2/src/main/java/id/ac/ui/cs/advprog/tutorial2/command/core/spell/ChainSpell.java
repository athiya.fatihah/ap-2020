package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    ArrayList <Spell> lstSpell;
    public ChainSpell(ArrayList lstSpell){
        this.lstSpell = lstSpell;
    }

    @Override
    public void cast(){
        for(Spell spl : lstSpell){
            spl.cast();
        }
    }

    @Override
    public void undo(){
        for(int i = this.lstSpell.size()-1; i >= 0; i--){
            this.lstSpell.get(i).undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
