package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Shield extends Weapon {
        //TODO: Complete me
        public Shield(){
            this.weaponValue = 10;
            this.weaponName = "Shield";
            this.weaponDescription = "Shield";
        }
}
