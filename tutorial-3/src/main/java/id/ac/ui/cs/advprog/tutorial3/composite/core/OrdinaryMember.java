package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class OrdinaryMember implements Member {
        //TODO: Complete me
        private String name;
        private String role;

        public OrdinaryMember(String name, String role){
            this.name = name;
            this.role = role;
        }

        @Override
        public void addChildMember(Member member) {
            // Do Nothing
        }

        public void removeChildMember(Member member){
            // Do Nothing
        }

        public List<Member> getChildMembers(){
            return new ArrayList<Member>();
        }

        public String getName(){
            return name;
        }

        public String getRole(){
            return role;
        }
}
