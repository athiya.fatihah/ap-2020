package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
        //TODO: Complete me
        private List<Member> childMembers;
        private String name;
        private String role;

        public PremiumMember(String name, String role){
            childMembers = new ArrayList<Member>();
            this.name = name;
            this.role = role;
        }

        @Override
        public void addChildMember(Member member) {
            if (this.role.equals("Master") || this.getChildMembers().size() < 3) {
                childMembers.add(member);
            }
        }

        public void removeChildMember(Member member){
            childMembers.remove(member);
        }

        public List<Member> getChildMembers(){
            return childMembers;
        }

        public String getName(){
            return name;
        }

        public String getRole(){
            return role;
        }
}
