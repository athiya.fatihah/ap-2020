package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GuildTest {
    private Guild guild;
    private Member guildMaster;

    @BeforeEach
    public void setUp() {
        guildMaster = new PremiumMember("Eko", "Master");
        guild = new Guild(guildMaster);
    }

    @Test
    public void testMethodAddMember() {
        //TODO: Complete me
        guild.addMember(guildMaster,new OrdinaryMember("Mina","Member"));
        assertEquals(2,guild.getMemberList().size());
    }

    @Test
    public void testMethodRemoveMember() {
        //TODO: Complete me
        guild.addMember(guildMaster,new OrdinaryMember("Shawn", "JuniorMaster"));
        guild.removeMember(guildMaster,guild.getMember("Shawn","JuniorMaster"));
        assertEquals(1,guild.getMemberList().size());
    }

    @Test
    public void testMethodMemberHierarchy() {
        Member sasuke = new OrdinaryMember("Asep", "Servant");
        guild.addMember(guildMaster, sasuke);
        assertEquals(guildMaster, guild.getMemberHierarchy().get(0));
        assertEquals(sasuke, guild.getMemberHierarchy().get(0).getChildMembers().get(0));
    }

    @Test
    public void testMethodGetMember() {
        //TODO: Complete me
        Member nicole = new OrdinaryMember("Nicole", "Researcher");

        guild.addMember(guildMaster, nicole);
        assertEquals(nicole, guild.getMember("Nicole","Researcher"));

    }
}
