package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        //TODO: Complete me
        assertEquals("Everest", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        assertEquals("Angel", member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me
        Member childMember = new OrdinaryMember("Rarity","Pony");
        member.addChildMember(childMember);
        assertEquals(1,member.getChildMembers().size());
    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        Member childMember = new OrdinaryMember("Rarity","Unicorn");
        member.addChildMember(childMember);
        member.removeChildMember(childMember);
        assertEquals(0,member.getChildMembers().size());

    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        member.addChildMember(new OrdinaryMember("Twilight","Unicorn"));
        member.addChildMember(new OrdinaryMember("PinkyPie","Pony"));
        member.addChildMember(new OrdinaryMember("RainbowDash","Pegasus"));
        member.addChildMember(new OrdinaryMember("AppleJack","Pony"));
        assertEquals(3,member.getChildMembers().size());
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member master = new PremiumMember("Celestia","Master");
        master.addChildMember(new OrdinaryMember("Cadence","New Master"));
        master.addChildMember(new OrdinaryMember("Cadence","New Master"));
        master.addChildMember(new OrdinaryMember("Cadence","New Master"));
        master.addChildMember(new OrdinaryMember("Cadence","New Master"));
        master.addChildMember(new OrdinaryMember("Cadence","New Master"));
        assertEquals(5,master.getChildMembers().size());
    }
}
