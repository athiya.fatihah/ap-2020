package id.ac.ui.cs.advprog.tutorial1.observer.service;

import id.ac.ui.cs.advprog.tutorial1.observer.core.*;
import id.ac.ui.cs.advprog.tutorial1.observer.repository.QuestRepository;
import id.ac.ui.cs.advprog.tutorial1.observer.core.Adventurer;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GuildServiceImpl implements GuildService {
        private final QuestRepository questRepository;
        private final Guild guild;
        private final Adventurer agileAdventurer;
        private final Adventurer knightAdventurer;
        private final Adventurer mysticAdventurer;

        public GuildServiceImpl(QuestRepository questRepository) {
                this.questRepository = questRepository;
                this.guild = new Guild();
                //ToDo: Complete Me
                agileAdventurer = new AgileAdventurer(guild);
                knightAdventurer = new KnightAdventurer(guild);
                mysticAdventurer = new MysticAdventurer(guild);
                for (Quest quest : questRepository.getQuests().values()){
                        this.guild.addQuest(quest);
                }
        }

        //ToDo: Complete Me
        public void addQuest(Quest quest){
                if (this.questRepository.save(quest) != null){
                        this.guild.addQuest(quest);
                }
        }

        public List<Adventurer> getAdventurers(){
                return this.guild.getAdventurers();
        }
}
