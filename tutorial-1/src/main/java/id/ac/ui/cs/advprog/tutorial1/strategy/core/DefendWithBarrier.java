package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
        //ToDo: Complete me
    public String defend(){
        return "Protected by Barrier.";
    }

    public String getType(){
        return "barrier";
    }
}
