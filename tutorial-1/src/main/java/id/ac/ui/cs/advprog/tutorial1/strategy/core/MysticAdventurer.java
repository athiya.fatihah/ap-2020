package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class MysticAdventurer extends Adventurer {
    //ToDo: Complete me
    /*
    attack behaviour menggunakan Sihir
    dan defense behaviour Perisai.
    */
    public void AgileAdventurer(){
        setAttackBehavior(new AttackWithMagic());
        setDefenseBehavior(new DefendWithShield());
    }

    public String getAlias(){
        return "I am a Knight Adventurer!";
    }
}
