package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class KnightAdventurer extends Adventurer {
        //ToDo: Complete me
        /*
        attack behaviour menggunakan Pedang
        dan defense behaviour Armor.
         */
        public void AgileAdventurer(){
            setAttackBehavior(new AttackWithSword());
            setDefenseBehavior(new DefendWithArmor());
        }

    public String getAlias(){
        return "I am a Knight Adventurer!";
    }
}
