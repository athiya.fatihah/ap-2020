package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

        public MysticAdventurer(Guild guild) {
                this.name = "Mystic";
                //ToDo: Complete Me
                this.guild = guild;
                this.guild.add(this);
        }

        //ToDo: Complete Me
        public void update(){
                String questType = this.guild.getQuestType();
                if (questType.equals("D") || questType.equals("E")){
                        getQuests().add(this.guild.getQuest());
                }
        }
}
