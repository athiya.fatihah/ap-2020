package id.ac.ui.cs.advprog.tutorial1.strategy.service;

import id.ac.ui.cs.advprog.tutorial1.strategy.core.*;
import id.ac.ui.cs.advprog.tutorial1.strategy.repository.AdventurerRepository;
import id.ac.ui.cs.advprog.tutorial1.strategy.repository.StrategyRepository;
import org.springframework.stereotype.Service;

@Service
public class AdventurerServiceImpl implements AdventurerService {

    private final AdventurerRepository adventurerRepository;

    private final StrategyRepository strategyRepository;

    public AdventurerServiceImpl(AdventurerRepository adventurerRepository,
                                 StrategyRepository strategyRepository) {

        this.adventurerRepository = adventurerRepository;
        this.strategyRepository = strategyRepository;
    }

    @Override
    public Iterable<Adventurer> findAll() {
        return adventurerRepository.findAll();
    }

    @Override
    public Adventurer findByAlias(String alias) {
        return adventurerRepository.findByAlias(alias);
    }

    @Override
    public void changeStrategy(String alias, String attackType, String defenseType) {
        //ToDo: Complete me
        Adventurer adventurer = findByAlias(alias);
        /*AttackBehaviour attackBehaviour = strategyRepository.getAttackBehaviourByType(attackType);
        DefenseBehaviour defenseBehaviour = strategyRepository.getDefenseBehaviour(defenseType);

        adventurer.setAttackBehaviour(attackBehaviour);
        adventurer.setDefenseBehaviour(defenseBehaviour);
         */
        if (attackType.equals("gun")){
            adventurer.setAttackBehavior(new AttackWithGun());
        }
        else if (attackType.equals("magic")){
            adventurer.setAttackBehavior(new AttackWithMagic());
        }
        else if (attackType.equals("sword")){
            adventurer.setAttackBehavior(new AttackWithSword());
        }
        else if (defenseType.equals("armor")){
            adventurer.setDefenseBehavior(new DefendWithArmor());
        }
        else if (defenseType.equals("barrier")){
            adventurer.setDefenseBehavior(new DefendWithBarrier());
        }
        else if (defenseType.equals("shield")){
            adventurer.setDefenseBehavior(new DefendWithShield());
        }
    }

    @Override
    public Iterable<AttackBehavior> getAttackBehaviors() {
        return strategyRepository.getAttackBehaviors();
    }

    @Override
    public Iterable<DefenseBehavior> getDefenseBehaviors() {
        return strategyRepository.getDefenseBehaviors();
    }
}
