package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {
    public String attack(){
        //ToDo: Complete me
        return "Clang!";
    }
    public String getType(){
        return "sword";
    }
}
