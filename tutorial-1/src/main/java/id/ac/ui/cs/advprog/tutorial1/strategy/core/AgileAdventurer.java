package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AgileAdventurer extends Adventurer {
        //ToDo: Complete me
        /*
        attack behaviour menggunakan Senjata Api
        dan defense behaviour Penghadang(Barrier).
         */
        public void AgileAdventurer(){
            setAttackBehavior(new AttackWithGun());
            setDefenseBehavior(new DefendWithBarrier());
        }

        public String getAlias(){
            return "I am an Agile Adventurer!";
        }
}
